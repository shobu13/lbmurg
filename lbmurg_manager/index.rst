.. lbmurg documentation master file, created by
   sphinx-quickstart on Thu Mar  5 17:21:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lbmurg's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Manager base modules
====================

.. automodule:: manager
  :members:
