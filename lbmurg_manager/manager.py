import asyncio
import uuid
from os import environ
import random

from autobahn import wamp
from autobahn.asyncio.wamp import ApplicationSession
from autobahn_autoreconnect import ApplicationRunner, BackoffStrategy
import redis

USER_EXPIRATION = 60
USER_REFRESH_RATE = 10
SONG_EXPIRATION = 60 * 60


class Component(ApplicationSession):
    """Base component to manage redis and clients data."""

    async def onJoin(self, details):
        """On join, register all subscriptions and remote procedure"""
        results = await self.subscribe(self)
        for res in results:
            if isinstance(res, wamp.protocol.Subscription):
                # res is an Subscription instance
                print("Ok, subscribed handler with subscription ID {}".format(res.id))
            else:
                # res is an Failure instance
                print("Failed to subscribe handler: {}".format(res))

        results = await self.register(self)
        for res in results:
            if isinstance(res, wamp.protocol.Registration):
                # res is an Registration instance
                print("Ok, registered procedure with registration ID {}".format(res.id))
            else:
                # res is an Failure instance
                print("Failed to register procedure: {}".format(res))

        asyncio.ensure_future(self.check_redis_messages())

    async def check_redis_messages(self):
        """Check if there is any redis message in queue, listen only keyevent
        for key expiration"""
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe("__keyevent@0__:expired")

        while True:
            await asyncio.sleep(1)
            message = p.get_message()
            if message:
                if message["data"].startswith("user"):
                    self.disconnect_user(message["data"].split(":")[1])
                elif message["data"].startswith("queue"):
                    id = message["data"].split(":")[1]
                    r.lrem("queue", 0, id)

    @wamp.register("fr.lbmurg.user.disconnect")
    def disconnect_user(self, user_id, is_broadcaster=False):
        r.lrem("users", 0, user_id)
        self.publish("fr.lbmurg.user.disconnected", user_id)

        users_guids = r.lrange("users", 0, -1)
        users = [r.hgetall(f"user:{i}") for i in users_guids]
        if not sum(user['broadcaster'] == '1' for user in users):
            new_broadcaster = random.choice([i for i in users if i["broadcaster"] != '1'])
            r.hset(f'user:{new_broadcaster["id"]}', 'broadcaster', '1')
            self.publish("fr.lbmurg.user.new_broadcaster", new_broadcaster)

    @wamp.register("fr.lbmurg.user.connect")
    def connect_user(self, username):
        """remote procedure to add a new client to the redis database

        :param username: username of the user to create

        :type: string

        :returns: connected user

        :rtype: dictionary

        """
        guid = str(uuid.uuid4())
        user = {"id": guid, "username": username, "broadcaster": int(len(r.lrange("users", 0, -1)) == 0),
                "refresh_rate": USER_REFRESH_RATE}
        r.hmset(f"user:{guid}", user)
        r.rpush("users", guid)
        r.expire(f"user:{guid}", USER_EXPIRATION)
        self.publish("fr.lbmurg.user.connected", user)
        return user

    @wamp.register("fr.lbmurg.user.get-connected")
    def get_connected_users(self):
        """remote procedure to get all connected users stored in redis


        :returns: list of connected users

        :rtype: list of dictionnaries

        """
        users_guids = r.lrange("users", 0, -1)
        users = [r.hgetall(f"user:{i}") for i in users_guids]
        return users

    @wamp.register("fr.lbmurg.user.heartbeat")
    def heartbeat(self, id):
        """remote procedure to refresh the expiration time of a user

        :param id: guid of the user to refresh

        :type: string

        :returns: refreshed user or None

        :rtype: dictionary or None

        """
        if r.expire(f"user:{id}", USER_EXPIRATION):
            return r.hgetall(f"user:{id}")
        return None

    @wamp.register("fr.lbmurg.queue.get")
    def queue_get(self):
        """remote procedure to retrieve the current queue

        :returns: current queue

        :rtype: list of dictionary """
        queue_id = r.lrange("queue", 0, -1)
        queue = [r.hgetall(f"queue:{i}") for i in queue_id]
        return queue

    @wamp.register("fr.lbmurg.queue.add")
    def queue_add(self, song):
        """remote procedure to add a song to the queue, if there is no playing song in current queue,
        set the statut of the added song to 'playing' then publish the added song on 'fr.lbmurg.queue.added' channel

        :param song: song to add

        :type: dictionary

        """
        queue = self.queue_get()
        if not len([i for i in queue if i["status"] == "PLAYING"]):
            song["status"] = "PLAYING"

        song["id"] = str(uuid.uuid4())
        r.hmset(f"queue:{song['id']}", song)
        r.rpush("queue", song["id"])
        self.publish("fr.lbmurg.queue.added", [song])

    @wamp.register("fr.lbmurg.queue.next")
    def queue_next(self, current_song, next_song):
        """remote procedure to play the next song in the queue, set the statue of the current song
        to 'PASSED' and the status of the next song to 'PLAYING' then publish the current and the next song on
        'fr.lbmurg.queue.current' channel

        :param current_song: current playing song

        :type: dictionary

        :param next_song: next playing song

        :type: dictionary

        """
        if next_song:
            current_song["status"] = "PASSED"
            next_song["status"] = "PLAYING"
            r.hset(f"queue:{current_song['id']}", "status", current_song["status"])
            r.hset(f"queue:{next_song['id']}", "status", next_song["status"])
            self.publish(
                "fr.lbmurg.queue.current", {"current": current_song, "next": next_song}
            )
        else:
            current_song["status"] = "PASSED"
            r.hset(f"queue:{current_song['id']}", "status", current_song["status"])
            self.publish(
                "fr.lbmurg.queue.current", {"current": current_song, "next": next_song}
            )

        r.expire(f"queue:{current_song['id']}", SONG_EXPIRATION)


if __name__ == "__main__":
    redis_host = environ.get("REDIS_HOST", "localhost")
    r = redis.Redis(
        host=redis_host, port=6379, db=0, charset="utf-8", decode_responses=True
    )
    r.config_set("notify-keyspace-events", "Ex")
    url = environ.get("WAMP_URL", "ws://localhost:8080/ws")
    realm = "lbmurg"
    strategy = BackoffStrategy(initial_interval=0.5, max_interval=40, factor=2)
    runner = ApplicationRunner(url, realm, retry_strategy=strategy)

    loop = asyncio.get_event_loop()
    asyncio.ensure_future(runner.run(Component))
    loop.run_forever()
