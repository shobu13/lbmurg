import Cookie from 'js-cookie'

export const STATUS = {
  QUEUED: 'QUEUED',
  PLAYING: 'PLAYING',
  PASSED: 'PASSED'
}


export const state = () => ({
  user: undefined,
  users: []
})
export const mutations = {
  setUser(state, user) {
    state.user = user
  },
  clearUser(state) {
    state.user = undefined
  },
  setConnectedUsers(state, users){
    state.users = users
  },
  addConnectedUser(state, user){
    state.users.push(user)
  },
  removeConnectedUser(state, userId){
    state.users = state.users.filter(u => u.id !== userId)
  }
}
export const actions = {
  async login({ commit }, username, id = undefined) {
    let user = await this.$wamp.call('fr.lbmurg.user.connect', [username])
    commit('setUser', user)
  },
  async refreshUser({ commit, state }) {
    if (state.user) {
      let user = await this.$wamp.call('fr.lbmurg.user.heartbeat', [state.user.id])
      if (user) {
        commit('setUser', user)
        return
      }
      commit('clearUser')
      return
    }
    commit('clearUser')
  }
}
