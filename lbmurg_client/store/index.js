import { createGuid } from '../utils/GUID'

export const STATUS = {
  QUEUED: 'QUEUED',
  PLAYING: 'PLAYING',
  PASSED: 'PASSED'
}


export const state = () => ({
  playlist: [],
  env: {}
})

export const getters = {
  getPlayingSong(state) {
    return state.playlist.find(s => s.status === STATUS.PLAYING)
  },
  getQueuedSongs(state) {
    return state.playlist.filter(s => s.status === STATUS.QUEUED)
  },
  getPassedSongs(state) {
    return state.playlist.filter(s => s.status === STATUS.PASSED)
  },
  getNextPlayingSong(state) {
    let queue = state.playlist.filter(s => s.status === STATUS.QUEUED)
    if (queue.length) {
      return queue[0]
    } else {
      return null
    }
  }
}

export const mutations = {
  addSong(state, song) {
    state.playlist.push(song)
    return song
  },
  updateSong(state, song) {
    let songToUpdate = state.playlist.find(s => s.id === song.id)
    Object.assign(songToUpdate, song)
  },
  setQueue(state, queue) {
    state.playlist = queue
  },
  setEnv (state, env) {
    state.env = env
  }
}

export const actions = {
  async fetchQueue({ commit }) {
    let queue = await this.$wamp.call('fr.lbmurg.queue.get')
    commit('setQueue', queue)
  },
  nuxtServerInit ({ commit }) {
      commit('setEnv', {
        VERSION: process.env.VERSION
      })
  }
}
