import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      paths: ['auth.user'],
      rehydrated: async (store) => {
        await store.dispatch('auth/refreshUser')
      }
    })(store)
  })
}
